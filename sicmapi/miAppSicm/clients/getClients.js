// con esta funcion se puede generar la busqueda general en toda la colleccion de datos de mongo
const Cliente = require('../../models/Client');

function getClients(req , res){
Cliente.find({})
  .then(function(doc){
      res.json({
          cliente:doc
      })
  })
  .catch(function(err){
      res.json({
          error: err
      })
  })
}

module.exports = getClients