const Cliente = require('../../models/Client');

function createClient(req, res){
    const idcliente = req.body.idcliente;
    const nombre = req.body.nombre;
    const apellido = req.body.apellido;
    const cuenta = req.body.cuenta;

    const newCliente = new Cliente({
        idcliente:idcliente,
        nombre:nombre,
        apellido:apellido,
        cuenta:cuenta
    });

    newCliente.save()
    .then(function(result){
      res.json({
        message: 'Cliente Creado'
      })
    })
    .catch(function(err){
      res.json({
        error: err
      })
    })

}

module.exports = createClient;