const express = require('express');
const router = express.Router();

const getClients = require('./clients/getClients');
const createClient = require('./clients/createClient');
const updateClient = require('./clients/updateClient');
const deleteClient = require('./clients/deleteClient');

router.get('/getClients',getClients);
router.post('/createClient',createClient);
router.put('/updateClient/:id',updateClient);
router.delete('/deleteClient/:id',deleteClient)

//post
//put
//delete
//wherever

module.exports = router;