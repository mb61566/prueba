const express = require('express');
const app = express();
const port = 8080;
const conectaBD = "mongodb://admin123:admin123@ds035553.mlab.com:35553/erodriguezg";
const mongose = require('mongoose');
const cors = require("cors");

app.use('/',express.static('../../sicm-api')); //utilizar todos los atributos estaticos de mi pagina web
app.use(cors());

const sicmapi = require("./sicmapi");
app.use('/sicmapi',sicmapi);

app.listen(port, function(){
    console.log(`inicia el servidor :${port}`);
    console.log(`http://localhost:${port}`); 
});

//Conector a la BD.
mongose.connect(conectaBD, { useNewUrlParser: true })
.then(function(result){
    console.log('se conecto mongo');
})
.catch(function(err){
    console.log(err);
});